<?php
include('header.html');
?>

<div class="contenido">
	<?php
		include('funciones.php');
		generarformulario("buscar.php");		
	?>

<?php
if(isset($_POST)){
    $and = false;
	$consulta = "SELECT * FROM contactos WHERE ";
	
	if($_POST['nombre'] != ""){
		$consulta = $consulta . "nombre = :nombre";
		$and=true;
	}
	if($_POST['apellidos']!= ""){
		if ($and){
			$consulta = $consulta ." AND  apellidos = :apellidos";
		}else{
			$consulta = $consulta ." apellidos = :apellidos";
			$and=true;
		}
	}
	if($_POST['email'] != ""){
		if ($and){
			$consulta = $consulta ." AND correo = :email";
		}else{
			$consulta = $consulta ." correo = :email";
			$and=true;
		}
	}
	if($_POST['telefono'] != ""){
		if ($and){	
			$consulta = $consulta ." AND telefono = telefono = :telefono";
		}else{
			$consulta = $consulta ." telefono = :telefono";
			$and=true;
		}
	}
	//echo "<br/>" .$consulta;
	try{
		$conn = new PDO('sqlite:agenda.sqlite'); //crea conexion y crea la bd si no existe
		$sentencia=$conn->prepare($consulta);
			
		if($_POST['nombre'] != ""){
			$sentencia->bindParam(":nombre", $_POST['nombre']);
		}
		if($_POST['apellidos']!= ""){
			$sentencia->bindParam(":apellidos", $_POST['apellidos']);
		}
		if($_POST['email'] != ""){
			$sentencia->bindParam(":email", $_POST['email']);
		}
		if($_POST['telefono'] != ""){
			$sentencia->bindParam(":telefono", $_POST['telefono']);
		}
		$sentencia->execute();
		$resultado=$sentencia->fetchAll(PDO::FETCH_ASSOC);
		
		echo "<table class='contactos'>
				<th>
					<tr><td>Nombre</td><td>Apellidos</td><td>Telefono</td><td>Email</td></tr>
				</th>";
		foreach ($resultado as $contacto) {
			echo "<tr><td>", $contacto['nombre'],"</td>";
			echo "<td>", $contacto['apellidos'],"</td>";
			echo "<td>", $contacto['telefono'],"</td>";
			echo "<td>", $contacto['correo'],"</td></tr>";
		}
		echo "</table>";
		$conn = null;
	}catch(PDOException $e){
		echo $e -> getMessage();
	}
	
}
include('footer.html');

?>
</div>