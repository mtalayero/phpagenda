<?php
// Tamaño de los campos en la tabla
define("TAM_NOMBRE",     40);  // Tamaño del campo Nombre
define("TAM_APELLIDOS",  60);  // Tamaño del campo Apellidos
define("TAM_TELEFONO",   10);  // Tamaño del campo Teléfono
define("TAM_CORREO",     50);  // Tamaño del campo Correo

$crear_tabla = 'CREATE TABLE IF NOT EXISTS agenda (
    			id INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
    			nombre VARCHAR(" . TAM_NOMBRE . "), 
			    apellidos VARCHAR(" . TAM_APELLIDOS . "), 
    			telefono VARCHAR(" . TAM_TELEFONO . "),
    			correo VARCHAR(" . TAM_CORREO . "),   
    			PRIMARY KEY(id)
			) ';

try{
	// 1.- Crear base de datos (sqlite solo la ruta, mysql, ip, usuario y password)
	$conn = new PDO('sqlite:empleados.db'); //crea conexion y crea la bd si no existe
	$conn -> exec($crear_tabla); //crear tabla
	
}catch(PDOException $e ){
	echo $e->getMessage();
	
}
/*
 * LOG DE ERRORES: tail -f /var/log/apache2/error.log
 * 		POSIBLES ERRORES     ------> 	SOLUCIONES
 * could not find driver ------  1. apt-cache search php5 sqlite 
 * 							     2. sudo apt-get install php5-sqlite
 * 							     3. sudo gedit /etc/php5/apache2/php.ini
 * 								 4. buscar sqlite: sqlite=sqlite3.so al final
 * 								 5. sudo service apache2 reload
 
 * * Unable acces database       1. Apache entra como usuario otros: No tiene permisos
 * 								 2. Crear archivo a mano y darle permiso de escritura al arhivo
 * 									o dar permiso de escritura en el directorio para otros
 * 									chmod o+w  
 * 
 */
?>